# **Firewall**
- The Internet has made large amount of information available to the average computer user at home, in business and education.
-  The main consideration in this circumstance is network security, which firewalls provide.
-  the firewall acts as a go-between positioned between your computer and anything that tries to connect to it.

## **What is a Firewall?**
- A firewall is simply a software or hardware device that filters data.
coming through your internet connection into your private network or computer system.

## **How Firewall Works?**
- a firewall’s protection comes from monitoring and regulating traffic that goes in and out of your network. This is accomplished using a few different methods, including packet filtering, a proxy service, and stateful inspection.
- Properly designed and deployed, a firewall operates as a shield around your network just as skin on a human.
- A firewall functions by acting on traffic based on its policy. A policy is comprised of a set of rules. A rule is an action taken on traffic that fit a certain criteria.

![How Firewall works?](https://www.milesweb.co.uk/blog/wp-content/uploads/2019/12/HardwareFirewall.gif)

## **Types of Firewall**
  1.  ### *Pocket Filtering Firewall*
  2.  ### *Application / Proxy Firewall*
  3.  ### *Hybrid Firewall*


### **1. Pocket Filtering Firewall**
- During network communication, a node transmits a packet that is filtered and matched with predefined rules and policies. Once matched, a packet is either accepted or denied.
- Packet filtering checks source and destination IP addresses. If both IP addresses match, the packet is considered secure and verified. 
- Packet filtering is usually an effective defense against attacks from computers outside a local area network (LAN). As most routing devices have integrated filtering capabilities, packet filtering is considered a standard and cost-effective means of security.
        
#### Advantages :
- A single device can filter traffic for the entire network.
- Extremely fast and efficient in scanning traffic.
- Minimal effect on other resources, network performance and end-user experience.

#### Disadvantages :
- packet filtering lacks broader context that informs other types of firewalls.
- Doesn't check the payload and can be easily spoofed.
- Not an ideal option for every network.

### **2. Application / Proxy Firewall**
- A proxy firewall is like a mirror of your computer and detects malicious actors attempting to get through to your device.
- Proxy firewalls are a secure solution because of the separation they provide between your computer and the internet. 
- Attackers often need to connect directly to your computer to attack it. Because a proxy is between your computer and the internet, hackers cannot form a direct connection to it, rendering their attack useless. 
        
#### Advantages :
- Only processes requested transactions; all other traffic is rejected
- Easy to set up and manage.
- Low cost and minimal impact on end-user experience.
- While proxy firewall provide a higher level of security than packet filtering firewalls.

#### Disadvantages :
- If they aren't used in conjunction with other security technology, circuit-level gateways offer no protection against data leakage from devices within the firewall.
- No application layer monitoring.
- Requires ongoing updates to keep rules current.

### **3. Hybrid Firewall**
- Hybrid firewalls consist of multiple firewalls, each providing a specified set of functions.
- For instance, you can use one firewall to execute packet filtering while another firewall acts as a proxy. 
-  In this way, you can tweak the performance of your security system, taking advantage of the diverse range of capabilities the different firewalls offer.
-  It is used to create large networks.
        
#### Advantages :
- Can be modified as per requirement.
- It is extremely flexible.
- Error detecting and troubleshooting are easy.
- Handles a large volume of traffic.

#### Disadvantages :
- It is a type of network expensive.
- The design of a hybrid network is very complex.
- Installation is a difficult process
## Hardware firewall vs Software firewall
- ### Hardware Firewalls
  - A hardware firewall is a system that works independently from the computer it is protecting as it filters information coming from the internet into the system. 
  - If you have a broadband internet router, it likely has its own firewall.
  - To protect your system, a hardware firewall checks the data coming in from the various parts of the internet and verifies that it is safe.
  - Hardware firewalls that use packet filtering examine each data packet and check to see where it is coming from and its location.
  - A hardware firewall can protect all the computers attached to it, making it an easily scalable solution.


- ### Software Firewall
  - A software firewall is a program used by a computer to inspect data that goes in and out of the device. 
  - Like hardware firewalls, software firewalls filter data by checking to see if it—or its behavior—fits the profile of malicious code.
  - Software firewalls can monitor traffic trying to leave your computer as well, preventing it from being used to attack other networks or devices. 
  - A software firewall has to be installed on each computer in the network. Therefore, a software firewall can only protect one computer at a time.

## Conclusion 
- One of the most important security features of a firewall is that it prevents anyone on the outside from entering into a computer on your private network.
- While this is significant for businesses, most home networks won't likely be at risk in this way. However, installing a firewall offers some sense of security.

## Referances
- https://studymafia.org/firewall-seminar-report-with-ppt-and-pdf/
- https://www.fortinet.com/resources/cyberglossary/how-does-a-firewall-work
- https://www.techopedia.com/definition/4038/packet-filtering
- https://www.techtarget.com/searchsecurity/feature/The-five-different-types-of-firewalls
- https://www.fortinet.com/resources/cyberglossary/hybrid-firewall-advantages-disadvantages
- https://www.geeksforgeeks.org/advantages-and-disadvantages-of-hybrid-topology/
- https://www.youtube.com/playlist?list=PLBbU9-SUUCwV7Dpk7GI8QDLu3w54TNAA6
