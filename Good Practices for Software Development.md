# Good Practices for Software Development

## Question 1

1.  Make notes while discussing requirements with your team.
2.  Implementation taking longer than usual due to some unexpected issue - Inform relevant team members.
3.  Explain the problem clearly, mention the solutions you tried out to fix the problem.
4.  Make time for your company, the product you are working on, and your team members. This will help a lot in improving your communication with the team.
5.  Be available when someone replies to your message. Things move faster if it's a real-time conversation as compared to a back and forth every hour.
6.  Work when you work, play when you play is a good thumb rule.

## Question 2

- I think I need to improve on my software development skills.
- To make progress in this area, I can take classes to learn more about coding, practice working with different programming languages, and attend seminars and workshops related to software development.
- Additionally, I can read books and articles on the topic and actively practice coding on my own.
