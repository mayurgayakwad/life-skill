# Prevention of Sexual Harassment
## What kinds of behaviour cause sexual harassment?
- Talking about sex
- Telling obscene jokes
- Using endearments, crude or offensive language
- Unsolicited or unwanted touching of any part of clothing or body or
- commenting on someone's physical appearance
- Talking about /spreading rumours about someone’s sexual orientation or sex life
- Showing sexually suggestive pictures, notes, magazines or cartoons
- Using sexually suggestive gestures
- Staring, cornering, following, blocking the pathway, or stalking
- Persistent requests for dates, outings, lunches, dinners etc

## What would you do in case you face or witness any incident or repeated incidents of such behaviour?
- I will keep all records in sequence (events with date, time etc.) along with nature of the incident and description of those involved.
- Any claims of sexual harassment will be thoroughly and discreetly investigated by me, and if necessary, I'll take the necessary corrective action.