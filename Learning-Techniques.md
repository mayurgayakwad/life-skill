#  Learning Techniques
## 1. How to Learn Faster with the Feynman Technique
### Question 1:  What is Feynman Technique?
- The Feynman Technique is a learning strategy that maximises your potential and forces you to gain a thorough understanding of a subject.

### What are the different ways to implement this technique?
- Feynman Technique is very simple to understant any concept with simple step to fallows these are -
- i. If we want to understant concept write down on the top of piece of paper.
- ii. Explain this concept using simple language 
- iii. Identify the problem areas and then go back the sorce to review.
- iv. Pinpoint any complicated terms and challenge yourself simplify them.








## 2. Learning How to Learn TED talk by Barbara Oakley
### Summary: 
- Basically our brain works in two mode
    1. Focus mode
    2. Relax mode
- We should alternate between these modes as we learn new concepts.
- When we become stuck on a problem, we should turn off focus mode and take a break to reframe the issue.
- Use the Pomodore technique to avoid procrastination.
- The Pomodoro Technique advises working with focused attention for 25 minutes while disconnecting all outside distractions. After that, have some fun for a while.
- To learn effectively practice and exercise learning and remembering.
- Always test yourself and give yourself a mini-test.
- Try to recall things with closed eyes.

### What are some of the steps that you can take to improve your learning process?
1. We can use the Pomodoro technique for better learing.
2. We can test ourselves time by time to check our knowledge.
3. Recallings things with closed eyes.

## 3. Learn Anything in 20 hours
### Summary
- According to Josh Kaufman people get good at things with just a little bit of practice.
- Josh Kaufman said  "If you put 20 hours of focused deliberate practice into that things which you want to learn you will will be astounded".
- Josh Kaufman Explain 4-Methods for this these are -
  1. Deconstruct the skill
  2. Learn enough to self-correct
  3. Remove practice barries/distraction 
  4. Practice at least 20 hours

### What are some of the steps that you can while approaching a new topic?

1. Divide skill into smaller parts
2. Learn enough to self correct
3. Practice at least 20 hours with focused 
