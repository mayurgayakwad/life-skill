# Tiny Habits

## 1. Tiny Habits - BJ Fogg

### Question 1
#### Your takeaways from the video (Minimum 5 points)
1. The key to creating lasting behavior change is to make it small and doable.
2. It is important to have a trigger for your new behavior.
3. Celebrate your success, no matter how small, to reinforce the behavior.
4. It is important to be patient and persistent when creating new habits.
5. Creating new habits can be fun and rewarding!

### Question 2
#### How can you use B = MAP to make making new habits easier?
- B = MAP stands for behavior, motivation, ability, and prompt. You can use this framework to make creating new habits easier by first identifying the behavior you want to change, then figuring out what motivation you have for change, what ability you have to do the new behavior, and finally creating a prompt to remind you to do the new behavior.

### Question 3
#### Why it is important to "Shine" or Celebrate after each successful completion of habit?
- It is important to "shine" or celebrate after each successful completion of a habit because it reinforces the behavior and helps you to feel good about yourself, which in turn makes it more likely that you will continue the behavior.

### Question 4
#### Your takeaways from the 1% Better Everyday video (Minimum 5 points)

1. Creating lasting behavior change is possible, but it takes time and effort.
2. The key to creating lasting behavior change is to make small, incremental changes.
3. It is important to be patient and persistent when trying to create new habits.
4. Celebrate your success, no matter how small, to reinforce the behavior.
5. Creating new habits can be fun and rewarding!

### Question 6
Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?
- The book's perspective on habit formation is that it is primarily about identity. That is, the book argues that the key to creating lasting behavior change is to first change your identity, which then changes your habits and finally your outcomes. The book provides a step-by-step process for how to do this. 
- First, you need to figure out what sort of person you want to be. 
- Second, you need to adopt the beliefs and values of that person. 
- Third, you need to start taking actions that are aligned with those beliefs and values. 
- And finally, you need to keep taking those actions until they become habits.

### Question 7
Write about the book's perspective on how to make a good habit easier?
- The book's perspective on making a bad habit more difficult is that it is important to make the new, desired behavior more attractive than the old, undesired behavior.
- To do this, you need to find a way to make the new behavior more rewarding, either in the short-term or the long-term.
- In the short-term, you can make the new behavior more rewarding by finding a way to make it easier to do (such as by setting up a trigger or cue). 
- In the long-term, you can make the new behavior more rewarding by finding a way to make it more satisfying (such as by linking it to a positive outcome).

## 5. Reflection:
### Question 9:
Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
- I would like to exercise more. 
- To make this happen, I can set up a trigger or cue, such as placing my workout clothes next to my bed so that I see them when I wake up in the morning.
- I can also make the habit more attractive by finding a type of exercise that I enjoy, such as going for a walk in the park. 
- Finally, I can make the habit more satisfying by setting a goal for myself, such as to lose weight, get in shape, or improve my overall health. 

### Question 10:
Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
- I would like to eat less junk food. 
- I can make the habit unattractive by finding healthy alternatives that I enjoy more, such as fruits, vegetables, or whole grain snacks. 