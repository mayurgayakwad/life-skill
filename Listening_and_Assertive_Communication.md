# Listening and Active Communication
## 1. Active Listening
### What are the steps/strategies to do Active Listening? (Minimum 6 points)
- Avoid getting distracted by your own thoughts.
- Focus on the speaker and topic.
- Avoid talking too soon after the other person.
- Let them finish and then respond.
- With your body language, convey that you are paying attention.
- During significant conversions, take notes.


## 2. Reflective Listening
### According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)
Fisher's model of reflective listening involves four key points:

1. Acknowledging: Acknowledging the speaker’s feelings, thoughts, and ideas.

2. Paraphrasing: Summarizing or paraphrasing the speaker’s feelings, thoughts, and ideas.

3. Supporting: Supporting the speaker’s feelings, thoughts, and ideas.

4. Questioning: Asking questions to further explore the topic and gain a better understanding.



## 3. Reflection
### What are the obstacles in your listening process?
- Sometimes I become sidetracked by my own ideas.
- I get distracted when people interrupt me.



### What can you do to improve your listening?
- I will practise active listening.
-  I will avoid distractions while listing. 

## 4. Types of Communication
- Passive Communication
- Aggressive Communication
- Passive Aggressive Communication
    frdeeitr m7ymar sgHRJKB 
### When do you switch to Passive communication style in your day to day life?
- I typically switch to a passive communication style when I am faced with criticism or a difficult situation that I need to address

### When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- I try to avoid passive-aggressive communication styles in my day-to-day life. I find that it often leads to unnecessary conflict, and I prefer to resolve conflicts through direct and honest communication.

### How can you make your communication assertive?
- Speaking with confidence to any one.
- Choosing the right tone for speaking.
- I will avoid qualifiers.
- While talking, I will stick to the point.
- I will take responsibility for any mistakes. 