# Grit and Growth Mindset
## 1. Grit
### Question 1
- The Grit Video encourages viewers to cultivate a mindset of perseverance and resilience to help them reach their goals.
-  It emphasizes the importance of having a consistent, long-term focus and not giving up when obstacles arise. 
-  It suggests that by having a strong "grit" mentality, individuals can move forward and achieve success.

### Question 2
My key takeaways from the Grit video are to: 
1. Have a consistent, long-term focus and never give up. 
2. Persevere in the face of adversity.
3. Develop a mindset of resilience and determination.
4. Take small steps and keep going until you reach your goals.

## 2. Introduction to Growth Mindset
### Question 1

- The Introduction to Growth Mindset Video explains how having a growth mindset can help individuals reach their goals.
-  It emphasizes the importance of believing in yourself, learning from mistakes, and being open to feedback. 
-  It suggests that through cultivating a positive attitude and being proactive, individuals can achieve success.

### Question 2 
My key takeaways from the Introduction to Growth Mindset video are to: 
1. Believe in yourself and have faith in your abilities.
2. Learn from your mistakes and use them as opportunities for growth.
3. Be open to feedback and take it as a chance to improve.
4. Take ownership of your actions and be proactive.

## 3. Understanding Internal Locus of Control
### What is the Internal Locus of Control? What is the key point in the video?
- The Internal Locus of Control is the belief that individuals have control over their lives and the power to shape their own destiny.
-  The key point in the video is that having an internal locus of control can help individuals take ownership of their actions and be proactive in achieving their goals.

## 4. 3. How to build a Growth Mindset
### Question 1
- The Introduction to Growth Mindset Video encourages viewers to cultivate a mindset of growth and self-improvement. 
- It emphasizes the importance of believing in yourself, learning from mistakes, and being open to feedback. 
- It suggests that by developing a positive attitude and taking ownership of your actions, you can reach success.
### Question 2
My key takeaways from the Introduction to Growth Mindset video are to: 
1. Believe in yourself and have faith in your abilities.
2. Learn from your mistakes and use them as opportunities for growth.
3. Be open to feedback and take it as a chance to improve.
4. Take ownership of your actions and be proactive.
## 5. Mindset - A MountBlue Warrior 
- I know more efforts lead to better understanding.
- I will stay relaxed and focused no matter what happens.
- I will always be enthusiastic. I will have a smile on my face when I face new challenges or meet people.

