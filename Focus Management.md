# Focus Management
## Question 1
### What is Deep Work?
Deep Work is a term coined by Cal Newport, a computer scientist and author, to describe a type of work that requires intense focus and concentration. It is an important skill for those in knowledge-based industries, such as programmers, engineers, and scientists, and refers to activities such as coding, writing, problem solving, and creative thinking. Deep Work requires mental energy and is often done without any distractions.

## Question 2
### Paraphrase all the ideas in the above videos and this one in detail.
- The optimal duration for deep work depends on the individual, however, generally it is recommended to work for periods of no more than 90 minutes at a time, with regular breaks in between.
-  Deadlines can be beneficial for productivity, as they provide focus and motivation for the task at hand.
-  The Deep Work Book, by Cal Newport, is a guide to mastering the ability to focus without distraction on complex tasks.
-  It discusses the importance of deep work, including how to develop the skills needed to perform at higher levels, strategies for cultivating deep work, and the tools that can help with productivity.
-  Deep Work is a concept that emphasizes the importance of uninterrupted and undistracted focus.


## Question 3
### How can you implement the principles in your day to day life? 
-  Identifing the tasks that require deep work in workday and make them a priority. 
-  Turn off distractions such as notifications.
-  Settting specific, achievable goals for each task.
-  Taking regular breaks throughout the day to recharge and refocus.
-  Celebrating successes and review areas for improvement.
  
## Question 4 
### The key takeaways from the video on the dangers of social media are: 
1. Social media can be a major distraction, leading to decreased productivity and focus.
2. It can also lead to increased anxiety and depression, as well as other mental health issues.
3. Social media can be addictive and can lead to isolation, lack of sleep, and feelings of inadequacy.
4. Lead to comparison and envy, as well as feelings of dissatisfaction with one’s life.
5. Difficult to limit time spent on social media and to manage the content we consume. 
6. To protect our mental health and productivity, it is important to take breaks from social media, set limits on usage, and be mindful of the content we consume.