# Energy Management

## Question 1:
What are the activities you do that make you relax - Calm quadrant?
1. Reading
2. Listening to soothing music
3. Going for a walk
4. Taking a hot bath
5. Connecting with nature
6. Doing a creative activity
7. Spending quality time with friends and family
8. Practicing mindfulness
9. Enjoying a cup of tea
10. Doing a puzzle or other brain-teasing activity

## Question 2:
### When do you find getting into the Stress quadrant?
- Working long time 
- Trapped at work
- Not meeting deadlines
## Question 3:

### How do you understand if you are in the Excitement quadrant?

- Setting positive goals
- Doing something me enjoy
- Celebrating small wins
- Spending time with people who make me feel good



## Question 4
### Paraphrase the Sleep is your Superpower video in detail.
1. Sleep is essential for human health and wellbeing, and it is vital for our physical, mental, and emotional well-being.
2. Lack of sleep can lead to a variety of health problems, including mood swings, decreased alertness, and weakened immune system.
3. Getting enough sleep helps to improve focus and concentration, increase productivity, and enhance learning and memory.
4. Regularly getting adequate sleep helps to reduce stress levels and improve mood, while promoting a more positive outlook on life.
5. Quality sleep is important for restoring energy, repairing muscle tissue, and helping to regulate hormones.
6. Sleep also helps to boost creativity, increase problem-solving abilities, and promote better decision-making.
7. Studies have shown that sleeping well can help to reduce the risk of developing certain chronic diseases and disorders, including obesity, diabetes, and depression.
8. Creating a consistent sleep schedule can help you to get the most out of your rest, allowing you to wake up refreshed and energized.
9. Creating a sleep-friendly environment can help you to fall asleep more easily and stay asleep longer, allowing for more restful sleep.
10. Practicing good sleep hygiene, such as avoiding

## Question 5
### What are some ideas that you can implement to sleep better?
1. Establish a consistent bedtime routine: Go to bed and wake up at the same time every day, even on weekends and holidays.
2. Avoid blue light before bed: Turn off screens at least one hour before bedtime.
3. Keep your bedroom cool and dark: Make sure your bedroom is cool, dark, and quiet.
4. Exercise regularly: Exercise can help to improve the quality of your sleep.
5. Avoid caffeine and alcohol before bed: Avoid consuming caffeine and alcohol at least four hours before bedtime.
6. Avoid large meals before bed: Have a light snack if you’re feeling hungry before bed.
7. Take a hot bath or shower: Taking a hot bath or shower before bed can help to relax your body and mind.
8. Practice relaxation techniques: Try meditation, yoga, or deep breathing exercises to help you relax and prepare for sleep.


## Question 6:
### Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.

1. Exercise has a powerful effect on our brains, helping to improve focus, sharpen memory, and increase motivation. 
2. Regular physical activity can help to reduce stress and anxiety levels, while promoting a more positive outlook on life.
3. Exercise has been shown to increase the production of hormones and proteins that help to protect the brain and promote its growth.
4. Increased blood flow to the brain helps to provide vital nutrients and oxygen, allowing for greater mental clarity and acuity.
5. Exercise also helps to stimulate the production of endorphins, which can help to improve mood and provide a sense of well-being.

## Question 7
### What are some steps you can take to exercise more?

1. Create a realistic exercise plan, setting achievable goals and scheduling regular activity sessions.
2. Choose activities that you enjoy or find interesting, such as cycling, swimming, running, or yoga.
3. Exercising with a friend or a group can help to increase motivation and accountability.
4. Listen to music or watch a movie while you exercise to make the activity more enjoyable.
5. Set rewards for yourself when you reach your exercise goals.
6. Incorporate short bursts of exercise into your daily routine, such as taking the stairs instead of the elevator.
7. Make sure to get adequate rest between workouts to give your body time to recover.
8. Schedule regular breaks throughout the day to help prevent burnout.